.global add32

.global sub64

.global minU16

.global minS32

.global isLessThanU16

.global isLessThanS16

.global shiftLeftU16

.global shiftU32

.global shiftU32

.global shiftS8

.global isEqualU32

.global isEqualS8

 

.text

 

@ uint64_t add32(uint32_t x, uint32_t y)
@    R1,R0           R0          R1
add32:

    ADDS R0, R0, R1
    MOV R1, #0
    ADC R1, #0
    
    BX LR

 

@ uint64_t sub64(uint64_t x, uint64_t y)

@    R1,R0           R1,R0       R3,R2

sub64:

    SUBS R0, R0, R2

    SBC R1, R1, R3

    BX LR

@ uint16_t minU16(uint16_t x, uint16_t y)

@    R0                  R0         R1

minU16:

    CMP R0, R1

    BMI END

    BEQ END

    MOV R0, R1

 

END:

    BX LR
    
    
INC:
    MOV R0, #0

@ int32_t minS32(int32_t x, int32_t y)

minS32:

    CMP R0, R1

    BMI END

    BEQ END

    MOV R0, R1

 

@ bool isLessThanU16(uint16_t x, uint16_t y)

isLessThanU16:

    CMP R0, R1
    
    BPL INC

    MOV R0, #1


@ bool isLessThanS16(int16_t x, int16_t y)

isLessThanS16:

    CMP R0, R1

    MOV R0, #1

    BMI END

    BEQ END

    MOV R0, #0

 

 

@ uint16_t shiftLeftU16 (uint16_t x, uint16_t p)

shiftLeftU16:
    MOV R0, R0, LSL R1

    BX LR

 

@ uint32_t shiftU32 (uint32_t x, int32_t p)

shiftU32:
    CMP R1, #0
    MOVPL R0, R0, LSL R1
    NEGMI R1, R1
    MOVMI R0, R0, LSR R1
    BX LR


@ int8_t shiftS8(int8_t x, int8_t p)

shiftS8:
    CMP R1, #0
    MOVPL R0, R0, LSL R1
    NEGMI R1, R1
    MOVMI R0, R0, ASR R1
    BX LR


 

@ bool isEqualU32(uint32_t x, uint32_t y)

isEqualU32:

    CMP R0, R1

    MOV R0, #1

    BEQ END

    MOV R0, #0

@ This is the isequal Function that compares the signed integers input of x and y.
@ bool isEqualS8(int8_t x, int8_t y)

isEqualS8:

    CMP R0, R1

    MOV R0, #1

    BEQ END

    MOV R0, #0
    
    BX LR
