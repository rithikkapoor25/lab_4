.global add32
.global sub64
.global minU16
.global minS32
.global isLessThanU16
.global isLessThanS16
.global shiftLeftU16
.global shiftU32
.global shiftS8
.global isEqualU32
.global isEqualS8

.text
@ 1
add32:
	ADDS R0, R0, R1
	MOV R1, #0
	ADC R1,#0
	BX LR
@ 2
sub64:
	SUBS R0, R0, R2
	SBC R1, R1, R3
	BX LR

@ 3	
minU16:
	CMP R0, R1
	BCC end
	MOV R0, R1
	BX LR
@ 4
minS32:
	CMP R0, R1
	BLT end
	MOV R0, R1
	BX LR
@ 5
isLessThanU16:
	CMP R0, R1
	MOV R0, #1
	BCC end
	MOV R0, #0
	BX LR
@ 6
isLessThanS16:
	CMP R0, R1
	MOV R0, #1
	BLT end
	MOV R0, #0
	BX LR

end:
	BX LR
@ 7
shiftLeftU16:
	MOV R0, R0, LSL R1
	BX LR
@ 8
shiftU32:
	CMP R1, #0
	BLT lessthanU
	MOV R0, R0, LSL R1
	BX LR
	
lessthanU:
	MOV R2, #-1
	MUL R1, R1, R2
	MOV R0, R0, LSR R1
	BX LR
@ 9
shiftS8:
	CMP R1, #0
	BLT lessthanS
	MOV R0, R0, LSL R1
	BX LR

lessthanS:
	MOV R2, #-1
	MUL R1, R1, R2
	MOV R0, R0, ASR R1
	BX LR
@ 10
isEqualU32:
	CMP R0, R1
	MOV R0, #1
	BEQ end
	MOV R0, #0
	BX LR
@ 11
isEqualS8:
	CMP R0, R1
	MOV R0, #1
	BEQ end
	MOV R0, #0
	BX LR
