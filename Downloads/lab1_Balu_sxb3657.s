.global add32
.global sub64
.global minU16
.global minS32
.global isLessThanU16
.global isLessThanS16
.global shiftLeftU16
.global shiftU32
.global shiftS8
.global isEqualU32
.global isEqualS8

.text

@ 1
add32:
  ADDS R0, R0, R1
  MOV R1, #0
  ADC R1, #0
  BX LR

@ 2
sub64:
  SUBS R0, R0, R2
  SBC R1, R1, R3
  BX LR

@ 3
minU16:
  CMP R0, R1
  BCC END
  MOV R0, R1
  BX LR

END:
  BX LR

@ 4
minS32:
  CMP R0, R1
  BLT End
  MOV R0, R1
  BX LR

End:
  BX LR

@ 5
isLessThanU16:
  CMP R0, R1
  BCC END1
  MOV R0, #0
  BX LR

END1:
  MOV R0, #1
  BX LR

@ 6
isLessThanS16:
  CMP R0, R1
  BLT END2
  MOV R0, #0
  BX LR

END2:
  MOV R0, #1
  BX LR


@ 7
shiftLeftU16:
  MOV R0, R0, LSL R1
  BX LR

@ 8
shiftU32:
  CMP R1, #0
  BLT negative0
  MOV R0, R0, LSL R1
  BX LR
negative0:
  MOV R2, #-1
  MUL R1, R1, R2
  MOV R0, R0, LSR R1
  BX LR

@ 9
shiftS8:
    CMP R1, #0
    MOVPL R0, R0, LSL R1
    NEGMI R1, R1
    MOVMI R0, R0, ASR R1
    BX LR

@ 10
isEqualU32:
  CMP R0, R1
  BEQ END3
  MOV R0, #0
  BX LR
END3:
  MOV R0, #1
  BX LR

@ 11
isEqualS8:
  CMP R0, R1
  BEQ END4
  MOV R0, #0
  BX LR
END4:
  MOV R0, #1
  BX LR
