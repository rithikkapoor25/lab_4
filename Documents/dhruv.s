.global stringCopy
.global stringCat
.global sumS32
.global sumS16
.global sumU32_64
.global countNegative
.global countNonNegative
.global countMatches
.global returnMax
.global returnMin

.text

stringCopy:
	LDRB R2,[R1], #1
	
	STRB R2,[R0], #1
	
	CMP R2,#0
	
	BNE stringCopy
	
	BX LR
	
stringCat:

	MOV R2,R1
	
stringCatloop1:

	
	LDRB R3,[R2]
	ADD R2, R2, #1
	
	CMP R3,#0
	
	BNE stringCatloop1
	
	SUB R2,R2,#1
	
stringCatloop2:

	LDRB R3,[R0]
	ADD R0, R0, #1
	
	STRB R3,[R2]
	ADD R2, R2, #1
	
	CMP R3,#0
	
	BNE stringCatloop2
	
	MOV R0,R2
	
	BX LR


	
sumS32:

	MOV R2, R0
	
	MOV R0,#0
	
sumS32loop:

	CMP R1,#0
	
	BEQ sumS32end
	
	LDR R4,[R2]
	ADD R2, R2, #4
	
	ADD R0,R4
	
	SUB R1,R1,#1
	
	B sumS32loop

sumS32end:

	BX LR

sumS16:

	MOV R2, R0
	
	MOV R0,#0

sumS16loop:

	CMP R1,#0
	
	BEQ sumS16loopend
	
	LDRSB R4,[R2]
	ADD R2, R2, #2
	
	ADD R0,R4
	
	SUB R1,R1,#1
	
	B sumS16loop

sumS16loopend:

	BX LR

sumU32_64:

	MOV R2,R0
	
	MOV R3,R1 
	
	MOV R1,#0
	
	MOV R0,#0
	
sumU32_64_loop:

	CMP R3,#0
	
	BEQ sumU32_64_loopend
	
	LDR R4,[R2]
	ADD R2, R2, #4
	
	SUBS R3,R3,#1
	
	ADDS R0,R0,R4
	
	ADC R1,R1,#0
	
	B sumU32_64_loop
	
sumU32_64_loopend:

	BX LR
	
	
countNegative:

	MOV R2,R0
	
	MOV R0,#0
	
	
countNegativeLoop:

	LDRSH R3,[R2],#2
	
	CMP R3,#0
	
	ADDMI R0,R0,#1
	
	SUB R1,R1,#1
	
	CMP R1,#0
	
	BNE countNegativeLoop
	
	BX LR
	
countNonNegative:

	MOV R3,R0
	MOV R2,R0
	MOV R0,#0
	
countnonNegativeLoop:

	LDRSH R3,[R2]
	ADD R2, R2, #2
	
	CMP R3,#0
	
	ADDPL R0,R0,#1
	
	SUB R1,R1,#1
	
	CMP R1,#0
	
	BNE countnonNegativeLoop
	
	BX LR
	
	
@extern uint32_t countMatches(char str[], char toMatch);
	
countMatches:
	MOV R3, #0
loop:
	LDRB R2,[R0], #1
	CMP R2, #0
	BEQ exit
	CMP R2, R1
	ADDEQ R3, R3, #1
	B loop

exit:
	MOV R0, R3
	BX LR
	
	
	
returnMax:

	LDRSH R3,[R0], #2

	MOV R2, R3
	
returnMax_loop:

	CMP R1, #0 @ Check counter
	
	BEQ returnMax_end

	LDRSH R3,[R0], #2

	SUB R1, R1, #1 @ Decrement counter
	CMP R2, R3 
	MOVLT R2, R3
	
	B returnMax_loop
	
Change1:

	MOV R2, R3
	
returnMax_end:

	MOV R0, R2
	
	BX LR

	
	
@extern int32_t returnMin(int16_t x[], uint32_t count);

returnMin:
	LDRSH R3,[R0], #2

	MOV R2, R3
	
returnMin_loop:

	CMP R1, #0 @ Check counter
	
	BEQ returnMin_end

	LDRSH R3,[R0], #2

	SUB R1, R1, #1 @ Decrement counter
	CMP R2, R3 
	MOVGT R2, R3
	
	B returnMin_loop

Change2:

	MOV R2, R3
	
returnMin_end:

	MOV R0, R2
	
	BX LR
