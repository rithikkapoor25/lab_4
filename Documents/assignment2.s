.global stringCopy
.global stringCat
.global sumS32
.global sumS16
.global sumU32_64
.global countNegative
.global countNonNegative
.global countMatches
.global returnMax
.global returnMin

.text

stringCopy:
loop:
	LDRB R2, [R1], #1
	STRB R2, [R0], #1
	CMP R2, #0
	BNE loop
	BX LR

stringCat:
	MOV R2, R1
cat_loop1:
	LDRB R3, [R2], #1
	CMP R3, #0 
	BNE cat_loop1
	SUB R2, R2, #1
cat_loop2:
	LDRB R3, [R0], #1 
	STRB R3, [R2], #1
	CMP R3, #0 
	BNE cat_loop2
	BX LR

sumS32:
	MOV R2, R0
	MOV R0, #0
	
sum_loop:
	LDR R3, [R2], #4
	ADD R0, R0, R3
	SUBS R1, R1, #1 
	BNE sum_loop
	BX LR
	
sumS16:
	MOV R2, R0
	MOV R0, #0
	
sum_loop2:
	LDRSH R3, [R2], #2
	ADD R0, R0, R3
	SUBS R1, R1, #1 
	BNE sum_loop2
	BX LR
	
sumU32_64:
	MOV R2, R0
	MOV R0, #0
	MOV R4, R1
	MOV R1, #0
	
sum_loop3:
	LDR R3, [R2], #4
	ADDS R0, R0, R3
	ADC R1, R1, #0
	SUBS R4, R4, #1 
	BNE sum_loop3
	BX LR
	
countNegative:
	MOV R2, R0
	MOV R0, #0
countN_loop:
	LDRSH R3, [R2], #2
	CMP R3, #0
	BLT inc
cont:
	SUBS R1, R1, #1
	CMP R1, #0
	BNE countN_loop
	BX LR	
inc:
	ADD R0, R0, #1
	B cont
	
countNonNegative:
	MOV R2, R0
	MOV R0, #0
countN_loop2:
	LDRSH R3, [R2], #2
	CMP R3, #0
	BGE inc2
cont2:
	SUBS R1, R1, #1
	BNE countN_loop2
	BX LR	
inc2:
	ADD R0, R0, #1
	B cont2

countMatches:
	MOV R2, R0
	MOV R0, #0
count_match:
	LDRB R3, [R2], #1
	CMP R3, R1
	BEQ inc3
cont3:
	CMP R3, #0
	BNE count_match
	BX LR
inc3:
	ADD R0, R0, #1
	B cont3
	
returnMax:
	MOV R2, R0
	MOV R0, #0
find_max:
	LDRSH R3, [R2], #2
	CMP R3, R0
	BGT inc4
cont4:
	SUBS R1, R1, #1
	BNE find_max
	BX LR
inc4:
	MOV R0, R3
	B cont4
	
returnMin:
	MOV R2, R0
	MOV R0, #0
find_min:
	LDRSH R3, [R2], #2
	CMP R3, R0
	BLT inc5
cont5:
	SUBS R1, R1, #1
	BNE find_min
	BX LR
inc5:
	MOV R0, R3
	B cont5
	

	

	
	
	

	


	
